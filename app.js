'use strict';

const express = require('express');
const app = express();
const request = require('request');
const MongoClient = require('mongodb').MongoClient;

const Qingqiu = require('./lib/qingqiu');
const http = require('http');
const Country = require('./country_controller');
//const qingqiu=new qingqiu(req.params.name);

//let url = 'mongodb://localhost:27017/hillwah';
let url = 'mongodb://172.16.1.11:27017/jiehaohe';
let freeURI = 'http://freegeoip.net/json/';
app.get('/', function (req, res) {
    res.send('Begin here!');
});

app.get('/ip/:ipAddr', function (req, res) {
    let ipAddr = req.params.ipAddr;
    let rsData = {};

    console.log(ipAddr);
    MongoClient.connect(url, function (err, db) {
        db.collection('ipData').find({
            ip: ipAddr
        }).each(function (err, doc) {
            if (doc) {
                console.dir(doc);
                res.json( { country: doc.country_name } );
				
			}else{
				Qingqiu(ipAddr, function(error, data){
					console.error(error);
					console.error(data);
					if (error) {
						console.error(error);
					}else{
						let ipData = JSON.parse(data);
                        try {
                            db.collection('ipData').insertOne(ipData);
                            res.json( { country: ipData.country_name } );
						}catch (e) {
                            console.log(e);
                        }
					}
				})
			
				//错误每请求一次都会写数据库，第三次必定报错，请使用源代码修复；9点了，不够时间帮你修复
                /*request(
                    {
                        url: freeURI + ipAddr,
                        method: "GET"
                    }, function (error, response, body) {
                        let statusCode = response.statusCode;
                        console.log("Status code:", statusCode);
                        if (error) {
                            console.log("error: ", error);
                        }

                        if (statusCode != 200) {
                            res.sendStatus(statusCode);
                        } else {
                            console.log('reponse headers: ', response.headers)
                            console.log('geoip body: ', body);

                            let ipData = JSON.parse(body);
                            try {
                                db.collection('ipData').insertOne(ipData);
                                res.json( { country: ipData.country_name } );
                            } catch (e) {
                                console.log(e);
                            }
                        }
                    }
                )*/
            }
        })
    })
});

app.listen(3033, function () {
    console.log('New magic happend with 3033...');
});
